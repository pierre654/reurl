<?php
// classe d'interface avec la base de donnees postgresql et singleton

class DB {
	
	private $db;

	/************************************************************************/
	//	Connexion à la base
	/************************************************************************/
	private function connect() {
		if ($this->db == null) {
			$PARAM_hote='localhost';
			$PARAM_nom_bd='';
			$PARAM_utilisateur='';
			$PARAM_mot_passe='';
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			try {
				$connexion = new PDO('mysql:host=' . $PARAM_hote . ';dbname=' . $PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe, $pdo_options);
				$this->db = $connexion;
			} catch (PDOException $e) {
				echo $e->getMessage();
			}
		}
		return $this->db;
	}

	/***************************************************************/
	// SELECT generique sur une table quelconque
	/***************************************************************/
	function select($requeteSQL, $tabParam = array()) {
		$tab = array ();
		try {
			$reqPrep = $this->connect()->prepare($requeteSQL);
			$reqPrep->execute($tabParam);
			while ($tuple = $reqPrep->fetch()) {
				array_push($tab, $tuple);
			}
			$reqPrep->closeCursor();
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $tab;
	}
	
	/***************************************************************/
	// SELECT generique sur une table quelconque avec un seul resultat
	/***************************************************************/
	function selectVal($requeteSQL, $tabParam = array()) {
		try {
			$reqPrep = $this->connect()->prepare($requeteSQL);
			$reqPrep->execute($tabParam);
			$res = $reqPrep->fetch();
			$reqPrep->closeCursor();
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $res;
	}

	/***************************************************************/
	// MAJ generique sur une table quelconque
	/***************************************************************/
	function maj($requeteSQL, $tabParam = array()) {
		try {
			$reqPrep = $this->connect()->prepare($requeteSQL);
			$reqPrep->execute($tabParam);
			$reqPrep->closeCursor();
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
	}
	
	/***************************************************************/
	// Retourne le dernier id généré
	// $seqName nécessaire avec postgresql
	//
	// De manière générale, un SERIAL implique la création d'une
	// séquence portant le nom de la table concaténée du
	// nom du champ et du suffixe _seq : table_idauto_seq
	/***************************************************************/
	function lastInsertedId($seqName = '') {
		try {
			$res = $this->connect()->lastInsertId($seqName);
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
		return $res;
	}
}
?>
