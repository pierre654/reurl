<?php
	define('SUBTITLE', 'Raccourcisseur d\'URL');
	define('BTN_SHORT', 'Raccourcir');
	define('BTN_COPY', 'Copier');
	define('PLEASE_ENTER_A_LONG_URL', 'Saisir une adresse URL longue...');
	define('FAIL_TO_CONNECT_TO_DATABASE', 'Erreur lors de la tentative de connexion à la base de données.');
	define('THIS_LINK_ISNT_IN_DATABASE', 'Désolé, mais cette adresse n\'est actuellement pas enregistrée dans notre base de données.');
	define('THERE_IS_CURRENTLY', 'Il y a actuellement ');
	define('LINKS_IN_THE_DATABASE', ' liens dans la base de données.');
	define('HOW_TO_GET_COPY_TO_CLIPBOARD_WITH_FIREFOX', 'Comment activer la fonction "copier" avec Firefox ?');
	define('HELP_CONTENT', 'Il faut autoriser Firefox à accéder au presse papier. Pour cela, ouvrez un onglet, et tapez "about:config" dans la barre d\'adresse, recherchez la clé signed.applets.codebase_principal_support et passez sa valeur à true. Vous pourrez désormais utiliser le bouton "Copier".');
	define('THIS_WEBSITE_REQUIRES_JAVASCRIPT', 'Ce site disposera de plus de fonctionnalités si Javascript est activé.');
	define('INVALID_ADRESS', 'Adresse URL invalide');
	define('IMPOSSIBLE_REQUEST', 'Requête impossible');
	define('ERROR_EXECUTING_PAGE', "Il y a eu une erreur durant l'exécution de la page.");
	define('SITE_INFO', "");